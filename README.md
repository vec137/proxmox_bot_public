## **ABOUT**

This is a _telegram_ bot for managing your Sunshine instances. It works over LAN and helps you manage your users and their privileges. You can manage session time per instance. Well, I guess that's all!

## **TODO**

- Rewrite connections and list and make them one func
- Add admin specific options (e. g. deauth clients and manage servers) [done]
- Add option to add clients or remove [done]
- Add option for setting timer [done]
- Add remind about time left for client (5 and 1 min) [done]
- Make bot to work with database [done]
- Add reply markup [done]
- Add /help command for user and admin [done]
- Add state monitoring of vms
- Add ability to use different credentials for different instances

## **This bot works with .yml files:**

- auth.yml \
  user: "username for sunshine"\
  passwd: "password for sunshine"\
  token: "tg bot token"

_You should have the same logins and password for each Sunshine server!!!_

- server.yml\
  vm1:
  - ip: your.external.ip.addr
  - local_ip: your.local.ip.addr
  - notify: 0 \<!-- (do not touch it) -->
  - port: your port \<!-- (should be the same external and local) -->
  - since: 0.0 \<!-- (do not touch it) -->
  - time: 0 \<!-- (and this too) -->
  - up: true \<!-- (and this) -->
  - user: 0.0 \<!-- (you know what you should not do) -->

\*_and so on..._
