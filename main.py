#!./venv/bin/python3
import logging
import sqlite3
import time
from threading import Thread

import requests
import yaml
from requests.auth import HTTPBasicAuth
from telebot import TeleBot, types


class Bot:
    def __init__(
        self,
        logfile: str = "debug.log",
        srvfile: str = "./server.yml",
        authfile="./auth.yml",
    ):
        logging.basicConfig(filename=logfile, encoding="utf-8", level=logging.DEBUG)

        with open(authfile) as f:
            b = yaml.safe_load(f)
        self.basic = HTTPBasicAuth(b["user"], b["passwd"])
        self.con = sqlite3.connect("server_db", check_same_thread=False)
        self.cur = self.con.cursor()
        self.cur.execute(
            "CREATE TABLE IF NOT EXISTS users (id PRIMARY KEY, allowed, admin)"
        )

        self.srvfile = srvfile
        self.logfile = logfile
        with open(self.srvfile, "r") as f:
            self.srv = yaml.safe_load(f)
        Thread(target=self.check, daemon=True).start()

        self.bot: TeleBot = TeleBot(b["token"])
        self.default_mk = types.ReplyKeyboardMarkup(row_width=4)
        self.default_mk.add(
            types.InlineKeyboardButton(text="/start"),
            types.InlineKeyboardButton(text="/list"),
            types.InlineKeyboardButton(text="/connect"),
            types.InlineKeyboardButton(text="/help"),
        )
        self.admin_mk = self.default_mk
        self.admin_mk.add(
            types.InlineKeyboardButton(text="/deauth"),
            types.InlineKeyboardButton(text="/timers"),
            types.InlineKeyboardButton(text="/chmod"),
        )

        @self.bot.message_handler(["start"])
        def start(message):
            if self.auth(message.chat.id) == -1:
                self.bot.send_message(
                    chat_id=message.chat.id,
                    text="Hello admin!",
                    reply_markup=self.admin_mk,
                )
            elif self.auth(message.chat.id) == 1:
                self.bot.send_message(
                    chat_id=message.chat.id,
                    text="Hello user!",
                    reply_markup=self.default_mk,
                )
            else:
                self.bot.send_message(chat_id=message.chat.id, text="Unauthorized :(")
                try:
                    if not self.cur.execute(
                        f"SELECT * FROM users WHERE id = {message.chat.id}"
                    ).fetchall():
                        self.cur.execute(
                            f"INSERT INTO users VALUES({
                                message.chat.id}, 0, 0)"
                        )
                        self.con.commit()
                except Exception as e:
                    logging.error(f'Caught error: "{e}"')

        @self.bot.message_handler(["list"])
        def list(message):
            s: str = ""
            k = 1
            for vm in self.srv:
                s += f"Server {k}:\n"
                k += 1
                if self.srv[vm]["up"]:
                    s += "up: yes\n"
                else:
                    s += "up: no\n"
                if self.srv[vm]["user"] == 0:
                    s += "busy: no\n"
                else:
                    s += "busy: yes\n"
                if self.srv[vm]["time"] == 0:
                    s += "time left: 0\n"
                else:
                    t: int = int(
                        int(self.srv[vm]["time"] - time.time() + self.srv[vm]["since"])
                        / 60
                    )
                    s += f"time left: {t} m\n"
                s += "\n"
            mk = self.default_mk
            if self.auth(message.chat.id) == -1:
                mk = self.admin_mk
            self.bot.send_message(chat_id=message.chat.id, text=s, reply_markup=mk)

        @self.bot.message_handler(["connect"])
        def connect(message):
            s = "Available servers:\n"
            for vm in self.srv:
                if self.srv[vm]["user"] == 0 and self.srv[vm]["up"]:
                    s += f"{self.srv[vm]['ip']}:{self.srv[vm]['port']}\n"
            s += "Add one of the servers in moonlight\n"
            s += "To get started just send me the pin from your moonlight client"
            mk = self.default_mk
            if self.auth(message.chat.id) == -1:
                mk = self.admin_mk
            self.bot.send_message(chat_id=message.chat.id, text=s, reply_markup=mk)

        @self.bot.message_handler(regexp=r"^[0-9]+$")
        def get_pin(message):
            pin: str = message.json["text"]
            mk = self.default_mk
            if self.auth(message.chat.id) == -1:
                mk = self.admin_mk
            if self.auth(message.chat.id) == 0:
                self.bot.send_message(
                    chat_id=message.chat.id,
                    text="Sorry, but you are not authorized!",
                )
                return
            for vm in self.srv:
                if self.srv[vm]["user"] == message.chat.id:
                    self.bot.send_message(
                        chat_id=message.chat.id,
                        text="You seem to be abusing server,\nIf not, contact the admin!",
                        reply_markup=mk,
                    )
                    return

            try:
                flag = False
                for vm in self.srv:
                    if self.srv[vm]["user"] == 0:
                        addr = f'https://{self.srv[vm]["local_ip"]
                                          }:{self.srv[vm]["port"] + 1}/api/pin'
                        res = requests.post(
                            addr,
                            auth=self.basic,
                            verify=False,
                            json={
                                "pin": message.json["text"],
                                "name": f"{self.srv[vm]['user']}",
                            },
                        ).json()["status"]
                        if res == "true":
                            self.srv[vm]["user"] = message.chat.id
                            self.srv[vm]["time"] = 60 * 60
                            self.srv[vm]["since"] = time.time()
                            self.bot.send_message(
                                chat_id=message.chat.id,
                                text="Pin is correct, have a nice play!",
                                reply_markup=mk,
                            )
                            flag = True
                            break
                if not flag:
                    self.bot.send_message(
                        chat_id=message.chat.id,
                        text="Pin is incorrect, try again!",
                        reply_markup=mk,
                    )
                with open(srvfile, "w") as f:
                    yaml.dump(self.srv, f)
            except:
                self.bot.send_message(
                    chat_id=message.chat.id,
                    text="Looks like something went wrong, contact the admin",
                    reply_markup=mk,
                )

        @self.bot.message_handler(["deauth"])
        def list_deauth(message):
            if self.auth(message.chat.id) != -1:
                self.bot.send_message(
                    chat_id=message.chat.id, text="You do not have admin rights!"
                )
                return
            s = ""
            mk = types.ReplyKeyboardMarkup()
            for vm in self.srv:
                if self.srv[vm]["user"] != 0:
                    s += f"{vm} is busy\n"
                else:
                    s += f"{vm} is not busy\n"
                mk.add(types.InlineKeyboardButton(text=vm))
            mk.add(types.InlineKeyboardButton(text="/help"))
            self.bot.send_message(chat_id=message.chat.id, text=s, reply_markup=mk)

        @self.bot.message_handler(regexp=r"^[A-Za-z0-9]+$")
        def deauth(message):
            if self.auth(message.chat.id) == -1:
                v = message.json["text"]
                for vm in self.srv:
                    if v == vm:
                        self.srv[vm]["user"] = 1e10
                        self.srv[vm]["time"] = 0
                        self.srv[vm]["since"] = 0.0
                        self.bot.send_message(
                            chat_id=message.chat.id,
                            text=f"Attempting to deauth {v}",
                            reply_markup=self.admin_mk,
                        )

        @self.bot.message_handler(["timers"])
        def timers(message):
            if self.auth(message.chat.id) == -1:
                for vm in self.srv:
                    t = int(self.time(vm) // 60)
                    if t < 0:
                        t = 0
                    self.bot.send_message(
                        chat_id=message.chat.id,
                        text=f"{vm} timer is set to {
                            self.srv[vm]['time'] // 60}m\nIt has {t} minutes left",
                        reply_markup=self.admin_mk,
                    )

        @self.bot.message_handler(regexp=r"^[0-9]+m [A-Za-z0-9]+$")
        def set_timer(message):
            if self.auth(message.chat.id) == -1:
                t = int(message.json["text"].split(" ")[0].split("m")[0])
                v = message.json["text"].split(" ")[1]
                f = False
                for vm in self.srv:
                    if v == vm and self.srv[vm]["user"] != 0:
                        self.srv[vm]["time"] = t * 60
                        self.srv[vm]["since"] = time.time()
                        with open(self.srvfile, "w") as f:
                            yaml.dump(self.srv, f)
                        self.bot.send_message(
                            chat_id=message.chat.id,
                            text=f"Successfully set timer to {t} min on {v}",
                            reply_markup=self.admin_mk,
                        )
                        f = True
                if not f:
                    self.bot.send_message(
                        chat_id=message.chat.id,
                        text="Couldn't set timer, check regexp or vm state!",
                        reply_markup=self.admin_mk,
                    )

        @self.bot.message_handler(["chmod"])
        def list_users(message):
            if self.auth(message.chat.id) == -1:
                u = self.cur.execute("SELECT * FROM users").fetchall()
                s = "[Users]\nid\tallowed\tadmin\n"
                for e in u:
                    s += f"{e[0]}\t{e[1]}\t{e[2]}\n"
                self.bot.send_message(
                    chat_id=message.chat.id, text=s, reply_markup=self.admin_mk
                )

        @self.bot.message_handler(regexp=r"^[0-9]+ [01] [01]$")
        def chmod(message):
            if self.auth(message.chat.id) == -1:
                id, allowed, admin = message.json["text"].split(" ")
                self.cur.execute(
                    f"UPDATE users SET allowed = {allowed} WHERE id = {id}"
                )
                self.cur.execute(f"UPDATE users SET admin = {
                                 admin} WHERE id = {id}")
                self.con.commit()
                self.bot.send_message(
                    chat_id=message.chat.id,
                    text="Successfully updated",
                    reply_markup=self.admin_mk,
                )

        @self.bot.message_handler(["help"])
        def help(message):
            s = "[?] You're displaying help message\n"
            s += "\n[Commands]\n"
            s += "\t/start - start the bot\n"
            s += "\t/list - list the servers load\n"
            s += "\t/connect - choose server to connect\n"
            s += "\n[RegExp]\n"
            s += "\t^[0-9]+$ - for pin\n"
            mk = self.default_mk
            if self.auth(message.chat.id) == -1:
                s += "\n[Admin]\n"
                s += "\t/deauth - list of servers\n"
                s += "\t/timers - list timers\n"
                s += "\t/chmod - list users\n"
                s += "\t^win[0-9]+$ - server to deauth\n"
                s += "\t^[0-9]+m win[0-9]$ - set timer on server\n"
                s += "\t^[0-9]+ [01] [01]$ - set user rights\n"
                mk = self.admin_mk
            self.bot.send_message(chat_id=message.chat.id, text=s, reply_markup=mk)

        self.bot.infinity_polling()

    def auth(
        self, id: int
    ) -> int:  # 0 for unauthorized, 1 for authorized, -1 for admin
        if self.cur.execute(
            f"SELECT * FROM users WHERE id = {id} AND admin = 1"
        ).fetchall():
            return -1
        elif self.cur.execute(
            f"SELECT * FROM users WHERE id = {id} AND allowed = 1"
        ).fetchall():
            return 1
        else:
            return 0

    def check(self) -> None:
        while True:
            for vm in self.srv:
                addr = f'https://{self.srv[vm]["local_ip"]
                                  }:{self.srv[vm]["port"] + 1}/api/'
                if self.srv[vm]["user"] != 0:
                    t = self.time(vm) / 60
                    if t <= 0:
                        logging.debug(f"Attempting to deauth {vm}")
                        requests.post(
                            addr + "clients/unpair-all",
                            verify=False,
                            auth=self.basic,
                        ).json()
                        logging.debug(f"Clients unpaired on {vm}")
                        try:
                            requests.post(
                                addr + "restart",
                                verify=False,
                                auth=self.basic,
                            )
                        except:
                            logging.debug(f"Deauth attempt on {vm}")
                            pass

                        self.srv[vm]["user"] = 0
                        self.srv[vm]["time"] = 0
                        self.srv[vm]["since"] = 0.0
                        self.srv[vm]["notify"] = 0
                        with open(self.srvfile, "w") as f:
                            yaml.dump(self.srv, f)
                        logging.debug(f"Deauth competed on {vm}")
                    elif t <= 5.0 and t > 1.0:
                        if self.srv[vm]["notify"] == 0:
                            self.bot.send_message(
                                chat_id=self.srv[vm]["user"],
                                text="You have less than 5 minutes left!",
                            )
                            self.srv[vm]["notify"] = 5
                    elif t <= 1.0:
                        if self.srv[vm]["notify"] == 5 or self.srv[vm]["notify"] == 0:
                            self.bot.send_message(
                                chat_id=self.srv[vm]["user"],
                                text="You have less than 1 minutes left!",
                            )
                            self.srv[vm]["notify"] = 1
                self.is_alive(vm)
                time.sleep(1)

    def time(self, vm: str) -> float:
        return self.srv[vm]["time"] - time.time() + self.srv[vm]["since"]

    def is_alive(self, vm) -> bool:
        try:
            addr = f'https://{self.srv[vm]["local_ip"]
                              }:{self.srv[vm]["port"] + 1}/'
            requests.get(addr, auth=self.basic, verify=False, timeout=5)
            self.srv[vm]["up"] = True
            with open(self.srvfile, "w") as f:
                yaml.dump(self.srv, f)
        except:
            logging.error(f"{vm} is down, timeout after 5 seconds")
            self.srv[vm]["up"] = False
            with open(self.srvfile, "w") as f:
                yaml.dump(self.srv, f)
            return False
        return True


Bot()
